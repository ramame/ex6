// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyB6uAvFX8oxQeZ7ZlVxoEqAP8TIJQQljvQ",
    authDomain: "fbreg-f595d.firebaseapp.com",
    databaseURL: "https://fbreg-f595d.firebaseio.com",
    projectId: "fbreg-f595d",
    storageBucket: "fbreg-f595d.appspot.com",
    messagingSenderId: "215638104729"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
