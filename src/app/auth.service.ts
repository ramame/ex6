import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth'
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

 
  signup(email:string, password:string){
    return this.fireBaseAuth.auth.createUserWithEmailAndPassword(email,password);
    }
  
    updateProfile(user,name:string){
    user.updateProfile({displayName:name,photoURL:''})
    }
  login(email:string, password:string){
    return this.fireBaseAuth.auth.signInWithEmailAndPassword(email,password).then(value => {console.log("succes to login")})
}

logout(){
  return this.fireBaseAuth.auth.signOut().then(value => {console.log("success logout")})
}

addUser(user, name:string, email:string){               //המילה צ'ילד מחפשת אינפוט אם הוא מוצא הוא כותב תחתיו ומתקדם אם הוא לא מוצא הוא מוסיף את האיי די וממשיך את הפקודות
let uid = user.uid;                      // לדוגמא אם היה דטה בייס ריק היה ברגע ההרצה של הקוד הזה היה נוצר המילה יוסרס ומתחתיו האיי די של המשתמש שיוצר ומתחתיו השם משתמש של אותו  משתמש            
let ref = this.db.database.ref('/');
ref.child('users').child(uid).push({'name':name, 'email':email}); 

}



user:Observable<firebase.User>; //לשאול את רוני על השורה הזאת
                                //בזמן שמנוי מחובר ויש לו תוקן אז זה קורה?


  constructor(private fireBaseAuth:AngularFireAuth, private db:AngularFireDatabase) { 
    this.user = fireBaseAuth.authState;

  }
}
