import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AuthService } from '../auth.service';

@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  toSignup(){
    this.router.navigate(['/signup']);
  }
  toWelcome(){
    this.router.navigate(['/welcome']);
  }
  toLogin(){
    this.router.navigate(['/login']);
  }
  toLogout(){
    this.Auth.logout().then(value =>{this.router.navigate(['/login'])}).catch(err => {console.log(err)})
  }

  constructor(private router:Router, public Auth:AuthService) { }

  ngOnInit() {
  }

}
