import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from "@angular/router";
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

email:string = '';
name:string= '';
password:string= '';
error ;
show1:boolean;
show2:boolean;
show3:boolean;
show5=false;
goodpasword:boolean;
click1:boolean;
username1:string;
errorcode:string;
errormessage:string;

toValid(){
if(this.name == '')
 this.show1=true
 if(this.email == '')
 this.show2=true
 if(this.password == '')
 this.show3=true
}


  toSignup(){
   this.toValid()
   if(!this.password.includes("@") ||!this.password.includes("#") ){
     this.show5=true;
   }
   if(this.password.includes("@") ||this.password.includes("#") ){
    this.show5=false;
     this.authService.signup(this.email,this.password)
    .then(value =>{
      this.authService.updateProfile(value.user,this.name);
      this.authService.addUser(value.user,this.name, this.email);
      console.log(value.user);
    
    }).then(value1 =>{ 
      this.router.navigate(['/welcome']);
    }).catch(err => {
       this.error = err;
       this.errorcode = err.code;
       this.errormessage =err.message;
       console.log(err);
      })
    }
    }
                                                  //זה לא אותו וליו מלפני
  constructor(private authService:AuthService, private router:Router) { }

 
 
  ngOnInit( ) {
    this.show1= false;
    this.show2= false;
    this.show3= false;
    this.goodpasword=false;
  
  }

}
