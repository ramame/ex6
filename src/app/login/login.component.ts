import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms'; 
import { AuthService } from '../auth.service';
import { Router } from "@angular/router";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    email:string;
    password:string;
    show1:boolean;
    errorcode:string;
    errormessage:string;
    required1:boolean;
    required2:boolean;

toLogin(){
  if(this.email == ''){
    this.required1=false
  }
  if(this.password == ''){
    this.required2=false
  }
  
  
  this.Auth.login(this.email,this.password).then(value=>{this.router.navigate(['/welcome'])})
  .catch(err => {
    console.log(err)
  this.errorcode = err.code;
  this.errormessage = err.message;
  })
}
  constructor(private Auth:AuthService, private router:Router) { }

  ngOnInit() {
   this.show1 = true;
  }

}
