import { Component, OnInit,Input } from '@angular/core';
import { TodosService } from '../todos.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  @Input() data:any;
  done;
  text;
  showUpdate = false;
  key;
  tempText ;
  constructor(private todos:TodosService, private db:AngularFireDatabase) { }
log(){
  console.log(this.done);
}
  update(){
    this.tempText = this.text;
    this.showUpdate = true
   
  }

  delete(){
    this.todos.deleteTodo(this.key)
    
  }
  cancelDelete(){
    this.showUpdate = false
    this.text = this.tempText
  }
  saveUpdate(){
    this.todos.updateTodo(this.key,this.text,this.done)
  }
  checked(){
    this.todos.updateTodo(this.key,this.text,this.done)
  }
  searchDoneTodo(done){
    return this.db.database
   }

   
  ngOnInit() {
    this.text = this.data.text;
    this.done = this.data.done;
    this.key= this.data.$key;
    
  }

}
