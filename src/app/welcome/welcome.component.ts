import { Component, OnInit,  } from '@angular/core';
import { AuthService } from '../auth.service';
import { AngularFireDatabase, } from '@angular/fire/database';
import { TodosService } from '../todos.service';
import * as _ from 'lodash';

@Component({
  selector: 'welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  sorting: string;
  sortodos:any[];
  todo = 'name of todo';
//array from firebase
  todos :any[];
  filtertodos:any[];
  //filter-able properties
  done:boolean;

  showall = true;

  filters = {};
  constructor(public Auth:AuthService, private db:AngularFireDatabase, private Todoo:TodosService) {}


  sendTodo(){
 
  this.Todoo.addTodo(this.todo)
  }

 orderDB(){
  this.showall=false;
  this.filtertodos = this.todos.filter(value=>value.done == false)
  
 } 

 showall1(){
  this.showall=true;
 }
 
  sort(){
    console.log('s')
   if(this.sorting == "1"){
   this.todos = this.todos.sort((a,b)=>b.done - a.done);}
   if(this.sorting == "2"){
    this.todos =  this.todos.sort((a,b)=>a.done - b.done);}
  }

  
  //filterTodos(){
    //this.Auth.user.subscribe(user=>{
  //    this.todos = this.db.list('/users/'+user.uid+'/todos', {
//query: {
//orderByChild: 'done',
//equalTo: this.done;

//}

        
    //  })})}

  

  ngOnInit() {
   
    this.Auth.user.subscribe(user => {                //read Data from DB
      this.db.list('/users/'+user.uid+'/todos').snapshotChanges().subscribe(
        todos =>{                   
        this.todos = [];                  
        todos.forEach(                   
        todo => {                         
        let y = todo.payload.toJSON();       
        y["$key"] = todo.key;                                                  
        this.todos.push(y); 
                 
    })})})
   // this.Auth.user.subscribe(user =>{
   // this.fillterdTodo = this.db.list('/users/'+user.uid+'/todos', {
   //   query: {
    //    orderByChild: 'text'
    //    equalTo: 'temp todo'
    //  }
   // }
 // })
     
    





  }

}