import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { updateBinding } from '@angular/core/src/render3/instructions';

@Injectable({
  providedIn: 'root'
})
export class TodosService {


  addTodo(todo){
    this.Auth.user.subscribe(user => {
      let ref = this.db.database.ref('/');
      ref.child('users').child(user.uid).child('todos').push({'text':todo, 'done':false}); 
    })
  }

  updateTodo(key, text, done){
    this.Auth.user.subscribe(user => {
      this.db.list('/users/'+user.uid+'/todos/').update(key, {'text' : text,'done':done });   
    })
  }
orderDB(done){
console.log("im inside the service")

}
  deleteTodo(key){
    this.Auth.user.subscribe(user => {
      this.db.list('/users/'+user.uid+'/todos/').remove(key);
    
    })
  }


  constructor(private Auth:AuthService, private db:AngularFireDatabase) { }
}
